use std::os;
use std::num;

fn main() {

   let args: ~[~str] = os::args();

   if args.len() != 4
   {
	println!("Usage: ./209sondage [pop] [echantillion] [sondage]")
   }
   else
   {
	let arg1: f32 = from_str(args[1]).unwrap();
	let arg2: f32 = from_str(args[2]).unwrap();
	let arg3: f32 = from_str(args[3]).unwrap();

	println!("Taille de la Population: {}", arg1)
	println!("Taille de l'échantillion: {}", arg2)
	println!("Résultat du sondage: {}%", arg3)
	println!("");

	let v: f32 = (((arg3 / 100.0) * (1.0 - (arg3 / 100.0))) / arg2) * ((arg1 - arg2) / (arg1 - 1.0));
	let t: f32 = ((2.0 * 1.96) * num::sqrt(v)) / 2.0 * 100.0;
	println!("Variance estimée: {}", v);
	println!("Intervalle de confiance à 95%: [{}%:{}%]", arg3 - t, arg3 + t);
	let f: f32 = ((2.0 * 2.58) * num::sqrt(v)) / 2.0 * 100.0;
	println!("Intervalle de confiance à 99%: [{}%:{}%]", arg3 - f, arg3 + f);
   }
}