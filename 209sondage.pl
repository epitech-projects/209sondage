#! /bin/perl

if ($#ARGV + 1 != 3)
{
    print "Usage: ./209sondage [pop] [echantillion] [sondage]" . "\n";
}
else
{
    print "Taille de la Population: $ARGV[0]" . "\n";
    print "Taille de l'échantillion: $ARGV[1]" . "\n";
    print "Résultat du sondage: $ARGV[2]%" . "\n";

    my $v = ((($ARGV[2] / 100) * (1 - ($ARGV[2] / 100))) / $ARGV[1])
	* (($ARGV[0] - $ARGV[1]) / ($ARGV[0] - 1));

    print "\n";
    print "Variance estimée: $v" . "\n";

    my $t = ((2 * 1.96) * sqrt($v)) / 2 * 100;
    print "Intervalle de confiance à 95%: " . ($ARGV[2] - $t) . " : " . ($ARGV[2] + $t) . "\n";

    my $t = ((2 * 2.58) * sqrt($v)) / 2 * 100;
    print "Intervalle de confiance à 95%: " . ($ARGV[2] - $t) . " : " . ($ARGV[2] + $t) . "\n";
}
