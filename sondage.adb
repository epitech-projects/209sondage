with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;

with Ada.Numerics.Elementary_Functions;
use Ada.Numerics.Elementary_Functions;

procedure Sondage is
v : Float := 0.0;
t : Float := 0.0;
begin
   if Argument_Count /= 3
   then Put("Usage: ./209sondage [pop] [echantillion] [sondage]");
   else
      Put("Taille De La Population: " & Argument(1));
      New_Line;
      Put("Taille De L'échantillion: " & Argument(2));
      New_Line;
      Put("Résultat du sondage: " & Argument(3) & "%");
      New_Line;
      v := (((Float'Value(Argument(3)) / 100.0) * (1.0 - (Float'Value(Argument(3)) / 100.0))) / Float'Value(Argument(2))) * ((Float'Value(Argument(1)) - Float'Value(Argument(2))) / (Float'Value(Argument(1)) - 1.0));
      Put("Variance estimée: ");
      Put(v, Exp => 0, Aft => 4);
      t := ((2.0 * 1.96) * (v**0.5)) / 2.0 * 100.0;
      New_Line;
      Put("Intervalle de confiance à 95%: [");
      if Float'Value(Argument(3)) - t < 0.0
      then
	 Put("0.0");
      else
	 Put(Float'Value(Argument(3)) - t, Exp => 0, Aft => 2);
      end if;
      Put("%:");
      if Float'Value(Argument(3)) + t > 100.0
      then
	 Put("100.0");
      else
	 Put(t + Float'Value(Argument(3)), Exp => 0, Aft => 2);
      end if;
      Put("%]");
      t := ((2.0 * 2.58) * (v**0.5)) / 2.0 * 100.0;
      New_Line;
      Put("Intervalle de confiance à 99%: [");
      if Float'Value(Argument(3)) - t < 0.0
      then
	 Put("0.0");
      else
	 Put(Float'Value(Argument(3)) - t, Exp => 0, Aft => 2);
      end if;
      Put("%:");
      if t + Float'Value(Argument(3)) > 100.0
      then
	 Put("100.0");
      else
	 Put(t + Float'Value(Argument(3)), Exp => 0, Aft => 2);
      end if;
      Put("]");
      New_Line;
   end if;
end Sondage;
