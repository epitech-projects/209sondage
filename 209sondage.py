#! /bin/python

import sys
from math import *

if (len(sys.argv) != 4):
    print ("Usage: ./209sondage [pop] [echantillion] [sondage]");
else:
    print ("Taille de la Population: ",sys.argv[1]);
    print ("Taille de l'échantillion: ",sys.argv[2]);
    print ("Résultat du sondage: ",sys.argv[3]);

    v = (((float(sys.argv[3]) / 100) * (1 - (float(sys.argv[3]) / 100))) / float(sys.argv[2])) * ((float(sys.argv[1]) - float(sys.argv[2])) / (float(sys.argv[1]) - 1));

    print ("\n");
    print ("Variance estimée: ",v);

    t = ((2 * 1.96) * sqrt(v)) / 2 * 100;
    print ("Intervalle de confiance à 95%: ",(float(sys.argv[3]) - t)," : ",(float(sys.argv[3]) + t));

    t = ((2 * 2.58) * sqrt(v)) / 2 * 100;
    print ("Intervalle de confiance à 95%: ",(float(sys.argv[3]) - t)," : ",(float(sys.argv[3]) + t));
